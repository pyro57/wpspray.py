# wpspray.py

Wordpress password sprayer, designed to go low and slow, avoid detection.

To use the scan functionality to gather usernames from authors on Wordpress
you will need to have wpscan installed, this is good to go on kali, but may
need to be installed on any other distro.

NOTE: currently is a work in progress, the program runs, but it has not been tested at all past that, USE AT YOUR OWN PERIL.

USAGE:
wpspray.py [url] [delay in minutes] # for defaults

wpspray.py [url] [delay in minutes] scan # to get usernames from wpscan

wpspray.py [url] [delay in minutes] [userfile] [password file] to specify both

wpspray.py [url] [delay in minutes] scan [password file] # to get usernames from wpscan and using a custom password file


Will ship with a default username and password file, but these have not been
created yet.  Will do farther testing.